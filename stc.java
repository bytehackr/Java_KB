
public class stc {

    int stack[] = new int[5];
    int top = 0;

    public void push(int data) {
        stack[top] = data;
        top++;
    }

    public void show() {
        for (int n : stack) {
            System.out.print(n + " ");
        }
    }

    public int pop() {
        int dato;
        top--;
        dato = stack[top];
        stack[top] = 0;
        return dato;
    }

    public int peek() {
        int data;
        data = stack[top - 1];
        return data;
    }

    public static void main(String[] args) {
        stc nums = new stc();
        nums.push(15);
        nums.push(10);
        nums.push(16);
        nums.show();

        System.out.println(nums.pop());
        System.out.println(nums.peek());

    }
}