package mypack;

import java.io.*;
public class Employee
{
	private int empid;
	private String ename;
	private double sal;
	
	BufferedReader br=new BufferedReader(new InputStreamReader(System.in));
	public void getData()
	{ 
		try
		{
			System.out.println("Enter Employee ID:");
			empid=Integer.parseInt(br.readLine());
			
			System.out.println("Enter Employee Name:");
			ename=br.readLine();
			
			System.out.println("Enter Employee Salary:");
			sal=Double.valueOf(br.readLine());
		}
		catch(Exception e)
		{
			System.err.println(e);
		}
	}
	
	public void shoWData()
	{
		System.out.println("\tEmployee Details\n");
		System.out.println("\tEmployee ID:"+empid);
		System.out.println("\tEmployee Name:"+ename);
		System.out.println("\tEmployee Salary:"+sal);
	}
}
