    public class JavaSetPriorityExp4 extends Thread  
    {    
        public void run()  
        {    
            System.out.println("running...");    
        }    
        public static void main(String args[])  
        {    
            JavaSetPriorityExp4 t1=new JavaSetPriorityExp4();    
            JavaSetPriorityExp4 t2=new JavaSetPriorityExp4();  
            t1.setPriority(4);  
            t2.setPriority(7);  
            System.out.println("Priority of thread t1 is: " + t1.getPriority()); //4  
            System.out.println("Priority of thread t2 is: " + t2.getPriority()); //7  
            t1.start();  
        }  
    } 
